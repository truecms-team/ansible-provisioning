# Welcome

Welcome to your wiki! This is the default page we've installed for your convenience. Go ahead and edit it.

## What is provisioned
- Nginx webserver, with Drupal, Wordpress or Wordpress multisite support
- MariaDB with user and database provisioning
- A user that to be used to SSH into the system and upload/provision website updates
- SSH keys
- PHP 5.6 or 7.0 support
- Ubuntu 16.04 or CentOS 7 support

## Cloning the repo

Go ahead and try:

```
$ git clone git@bitbucket.org:truecms-team/ansible-provisioning.git ~/ansible-provisioning
```

## How to provision
1. SSH into the provisioning server as non-privileged user (non-root)
2. Create ~/ansible-provisioning/ directory
3. Pull this repo into a sub-folder yourdomain.com (replace as required) `git clone https://truecms@bitbucket.org/truecms-team/ansible-provisioning.git yourdomain.com` 
4. Pull sub-modules `git pull --recurse-submodules && git submodule update --init --recursive`
5. Create hosts file with content `vim hosts`, with the following content:
```
[webserver]
ip-address-of-the-instance
```
6. Copy `grous_vars/default.all.yml` file into `grous_vars/all.yml` and update it's content replacing `<fixme> tokens
7. Execute ansible provisioning `ansible-playbook -i hosts ubuntu.yml`

## Resetting git submodules
`git submodule foreach git reset --hard`

## Install Linux Security
At the moment only Linux Malware Detect is recommended. To install, update or
create the allhosts file with IP addresses of the servers to run, ensure content
is
```
[webservers]
list IP addresses one per line
```

and execute this command
`ansible-playbook -i allhosts security.yml  --tags=maldet`